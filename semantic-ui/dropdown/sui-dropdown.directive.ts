import { Directive, HostListener, ElementRef, ViewChildren, QueryList, Output, EventEmitter, Input } from '@angular/core';
import {
  ActivatedRoute,
  Router,
  Event,
  RouteConfigLoadStart,
  RouteConfigLoadEnd,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';
import { SuiMenuUtil } from "../sui-menu.interface";

@Directive({
  selector: '[sui-dropdown]'
})
export class SuiDropdownDirective {

  showing: boolean;
  canShow = true;

  @Input() menu: ElementRef;
  @Output() opening = new EventEmitter();

  constructor(
    private elementRef: ElementRef,
    private router: Router) {
    router.events.subscribe((event: any) => {
      if (event instanceof RouteConfigLoadStart
        || event instanceof NavigationStart) {
        this.canShow = false;
        this.hideMenu();
      } else if (event instanceof NavigationEnd) {
        this.canShow = true;
      }
    });
  }

  @HostListener('click', ['$event'])
  clicked(evt: any) {
    evt.stopPropagation();
    if (this.showing) {
      this.hideMenu();
    } else
      if (this.canShow) {
        this.opening.emit(this);
        this.showMenu();
      }
  }

  @HostListener('document:click')
  documentClicked() {
    this.hideMenu();
  }

  private showMenu() {
    const menu = this.menu || this.elementRef.nativeElement.querySelector('.menu');
    if (menu) {
      this.showing = true;
      if (SuiMenuUtil.isInstance(menu)) {
        menu.showMenu();
      } else {
        menu.classList.add('transition');
        menu.classList.add('visible');
        menu.onclick = function (evt) {
          evt.stopPropagation();
        };
      }
    }
  }

  hideMenu() {
    const menu = this.menu || this.elementRef.nativeElement.querySelector('.menu');
    if (menu) {
      this.showing = false;
      if (SuiMenuUtil.isInstance(menu)) {
        menu.hideMenu();
      } else {
        menu.classList.remove('transition');
        menu.classList.remove('visible');
      }
    }
  }
}
export interface SuiMenu {

    toggleVisibility();

    showMenu();

    hideMenu();
}

export class SuiMenuUtil {

    static isInstance(value: any): boolean {
        if (('toggleVisibility' in value) &&
            ('showMenu' in value) &&
            ('hideMenu' in value)) {
            return true;
        }
        return false;
    }
}
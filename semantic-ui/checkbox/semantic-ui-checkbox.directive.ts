import { Directive, ViewChild, ElementRef, AfterViewInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[semanticUiChoiceButton]'
})
export class SemanticUiCheckboxDirective implements AfterViewInit {

  constructor(private rd: Renderer2, private _elementRef: ElementRef) {
  }

  ngAfterViewInit() {
    // console.log(this.rd);
    const input: any = this._elementRef.nativeElement.querySelector('input');
    const label: any = this._elementRef.nativeElement.querySelector('label');
    label.onclick = function () {
      input.click();
    };
  }

}

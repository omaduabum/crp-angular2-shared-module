import { Directive, ContentChildren, ViewRef, OnInit, QueryList, AfterContentInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import {SuiDropdownDirective} from "../dropdown/sui-dropdown.directive";

@Directive({
  selector: '[sui-dropdown-group]',
  exportAs: 'dropdownGroup'
})
export class SuiDropdownGroupDirective implements AfterContentInit {

  @ContentChildren(SuiDropdownDirective)
  dropdownList: QueryList<SuiDropdownDirective>;
  subscriptions: Subscription[] = [];

  constructor() {
  }

  ngAfterContentInit() {
    this.dropdownList.changes.subscribe(() => {
      this.subscriptions.forEach((it: Subscription) => it.unsubscribe());
      this.subscriptions = [];
      this.dropdownList
        .forEach((it: SuiDropdownDirective) => {
          this.subscriptions.push(it.opening.subscribe(() => this.closeOthers(it)));
        });
    });
  }

  closeOthers(dropdown: SuiDropdownDirective) {
    this.dropdownList
      .filter((it: SuiDropdownDirective) => it != dropdown)
      .forEach((it: SuiDropdownDirective) => it.hideMenu());
  }
}

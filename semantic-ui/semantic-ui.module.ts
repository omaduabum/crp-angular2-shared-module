import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SemanticUiCheckboxDirective } from './checkbox/semantic-ui-checkbox.directive';
import { SuiDropdownDirective } from './dropdown/sui-dropdown.directive';
import { SuiDropdownGroupDirective } from './dropdown-group/sui-dropdown-group.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SemanticUiCheckboxDirective, SuiDropdownDirective, SuiDropdownGroupDirective],
  exports: [SemanticUiCheckboxDirective, SuiDropdownDirective, SuiDropdownGroupDirective]
})
export class SemanticUiModule { }

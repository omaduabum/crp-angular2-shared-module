import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotfoundComponent} from './notfound/notfound.component';

const routes: Routes = [
  {path: '**', component: NotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  declarations: [NotfoundComponent],
  exports: [RouterModule]
})
export class NotfoundRoutingModule {}

export const routingComponents = [NotfoundComponent];

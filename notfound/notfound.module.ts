import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotfoundComponent } from './notfound/notfound.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '**', component: NotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  declarations: [NotfoundComponent],
  exports: [RouterModule]
})
export class NotfoundModule { }

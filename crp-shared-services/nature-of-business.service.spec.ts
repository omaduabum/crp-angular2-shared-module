import { TestBed, inject } from '@angular/core/testing';

import { NatureOfBusinessService } from './nature-of-business.service';

describe('NatureOfBusinessService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NatureOfBusinessService]
    });
  });

  it('should be created', inject([NatureOfBusinessService], (service: NatureOfBusinessService) => {
    expect(service).toBeTruthy();
  }));
});

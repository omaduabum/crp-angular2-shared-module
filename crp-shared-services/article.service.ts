import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';

import { SearchResponse } from '../util/search/SearchResponse.model';

@Injectable()
export class ArticleService {
    
    constructor( @Inject('API_URL') private API_URL: string, private http: HttpClient) { }
    get(registration: any, start: number, maxResult: number): Observable<Object> {
        return this.http.post(`${this.API_URL}/registration/${
            registration.company.availabilityCode
            }/article/search`, { start, length: maxResult })
            .map((obj: any) => new SearchResponse(obj));
    }
    findById(registration: any, id: number): Observable<Object> {
        return this.http.get(`${this.API_URL}/registration/${registration.company.availabilityCode
            }/article/${id}`);
    }
    restoreDefaultArticles(registration: any): Observable<any> {
        return this.http.post(`${this.API_URL}/registration/${registration.company.availabilityCode
            }/reset-articles`, {});
    }
    add(registration: any, data: any): Observable<any> {
        return this.http.post(`${this.API_URL}/registration/${registration.company.availabilityCode
            }/article`, data);
    }
    update(registration: any, articleId: number, data: any): Observable<any> {
        return this.http.patch(`${this.API_URL}/registration/${registration.company.availabilityCode
            }/article/${articleId}`, data);
    }
    removeArticle(registration: any, article: any): Observable<any> {
        return this.http.delete(`${this.API_URL}/registration/${
            registration.company.availabilityCode
            }/article/${article.id}`);
    }
    getArticleContents(registration: any, articleId: number): Observable<any> {
        return this.http.get(`${this.API_URL}/registration/${
            registration.company.availabilityCode
            }/article/${articleId}/content`);
    }
    // updateArticleContent(registration: any, article: any, contentId: number, data: any): Observable<any> {
    //   return this.http.patch(`${this.API_URL}/registration/${registration.company.availabilityCode
    //     }/article/${article.id}/content/${contentId}`, data);
    // }

}

import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { NameReservationReason } from '../model/name-reservation-reason.model';
import { PaymentSearchFilter } from '../model/payment-search-filter.model';
import { CompanyClassification } from '../model/company-classification.model';
import { SearchResponse } from '../util/search/SearchResponse.model';
import { environment } from 'app/../environments/environment';

@Injectable()
export class NameReservationService {

  reservationReasons: String[];

  constructor(private http: HttpClient, @Inject('API_URL') private API_URL: string) { }

  createNewNameReservation(data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/name-reservation`, data);
  }

  setNameReservationNameAndType(nameReservation: any, data: any): Observable<any> {
    return this.http.patch(`${this.API_URL}/name-reservation/${nameReservation.id}`, data);
  }

  setNameReservationPresenter(nameReservation: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/name-reservation/${nameReservation.id}/presenter`, data);
  }

  setNameReservationObjective(nameReservation: any, reason: NameReservationReason, data: any): Observable<any> {
    if (reason === NameReservationReason.NEW) {
      return this.http.post(`${this.API_URL}/name-reservation/${nameReservation.id}/objective/new-incorporation`, data);
    } else if (reason === NameReservationReason.SUBSIDIARY) {
      return this.http.post(`${this.API_URL}/name-reservation/${nameReservation.id}/objective/subsidiary`, data);
    }
    switch (reason) {
      case NameReservationReason.CONVERSION:
        data.updateType = 'CONVERSION';
        break;
      case NameReservationReason.CHANGE_OF_NAME:
        data.updateType = 'CHANGE_OF_NAME';
        break;
    }
    return this.http.post(`${this.API_URL}/name-reservation/${nameReservation.id}/objective/update-company`, data);
  }

  getSummary(): Observable<any> {
    return this.http.get(`${this.API_URL}/name-reservation/summary`);
  }

  getSpecialWords(data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/name-reservation/special-word-search`, data);
  }

  getNameReservations(filter: any, startIndex: number, length: number): Observable<SearchResponse> {
    filter.start = startIndex;
    filter.length = length;
    return this.http.post(`${this.API_URL}/name-reservation/search`, filter)
      .map((res: Object): SearchResponse => {
        return new SearchResponse(res);
      });
  }

  findById(id: number): Observable<any> {
    return this.http.get(`${this.API_URL}/name-reservation/${id}`);
  }

  fetchReservationReasons(companyClassification?: CompanyClassification): Observable<NameReservationReason[]> { 
    if (companyClassification && companyClassification.isIT()) { 
      return Observable.from([ 
        NameReservationReason.NEW, 
        NameReservationReason.CHANGE_OF_NAME 
      ]).toArray(); 
    } 
    return Observable.from([
      NameReservationReason.NEW,
      NameReservationReason.CHANGE_OF_NAME,
      NameReservationReason.CONVERSION,
      NameReservationReason.SUBSIDIARY
    ]).toArray();
  }

  getPayments(nameReservation: any, startIndex: number, length: number): Observable<any> {
    return this.http.post(`${this.API_URL}/name-reservation/${nameReservation.id}/payment/search`,
      { start: startIndex, length: length });
  }

  createSubmissionInvoice(nameReservation: any): Observable<any> {
    return this.http.post(`${this.API_URL}/name-reservation/${nameReservation.id}/payment`, {});
  }

  downloadStatusNotice(nameReservation: any): string {
    return `${environment.documentApiBaseUrl}/name-reservation/${nameReservation.id}/status-notice`;
  }

  downloadPaymentReceipt(nameReservation: any): string {
    return `${environment.documentApiBaseUrl}/name-reservation/${nameReservation.id}/payment-receipt`;
  }

}

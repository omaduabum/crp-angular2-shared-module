import { TestBed, inject } from '@angular/core/testing';

import { ProprietorService } from './proprietor.service';

describe('ProprietorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProprietorService]
    });
  });

  it('should be created', inject([ProprietorService], (service: ProprietorService) => {
    expect(service).toBeTruthy();
  }));
});

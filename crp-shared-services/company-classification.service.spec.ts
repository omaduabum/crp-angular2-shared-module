import { TestBed, inject } from '@angular/core/testing';

import { CompanyClassificationService } from './company-classification.service';

describe('CompanyClassificationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompanyClassificationService]
    });
  });

  it('should ...', inject([CompanyClassificationService], (service: CompanyClassificationService) => {
    expect(service).toBeTruthy();
  }));
});

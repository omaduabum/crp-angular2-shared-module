import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';

import { SearchResponse } from '../util/search/SearchResponse.model';

@Injectable()
export class ProprietorService {

  constructor( @Inject('API_URL') private API_URL: string, private http: HttpClient) { }

  get(registration: any, start: number, maxResult: number): Observable<Object> {
    return this.http.post(`${this.API_URL}/registration/${
      registration.company.availabilityCode
      }/proprietor/search`, { start, length: maxResult });
  }

}

import { TestBed, inject } from '@angular/core/testing';

import { CRPService } from './crp.service';

describe('CRPService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CRPService]
    });
  });

  it('should ...', inject([CRPService], (service: CRPService) => {
    expect(service).toBeTruthy();
  }));
});

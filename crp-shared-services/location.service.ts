import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, AsyncSubject, Observer } from 'rxjs';

@Injectable()
export class LocationService {

  private countries: AsyncSubject<any[]>;
  private nationalities: AsyncSubject<any[]>;
  private states: AsyncSubject<any[]>;

  constructor( @Inject('API_URL') private API_URL: string, private http: HttpClient) { }

  getNationalities(): Observable<any[]> {
    if (!this.nationalities) {
      this.http.get(`${this.API_URL}/nationality`)
        .subscribe(this.nationalities = new AsyncSubject());
    }
    return Observable.create((ob: Observer<any[]>) => {
      this.nationalities.subscribe(ob);
    });
  }

  getCountries(): Observable<any[]> {
    if (!this.countries) {
      this.http.get(`${this.API_URL}/country`)
        .subscribe(this.countries = new AsyncSubject());
    }
    return Observable.create((ob: Observer<any[]>) => {
      this.countries.subscribe(ob);
    });
  }

  getStates(): Observable<any[]> {
    if (!this.states) {
      this.http.get(`${this.API_URL}/state`)
        .subscribe(this.states = new AsyncSubject());
    }
    return Observable.create((ob: Observer<any[]>) => {
      this.states.subscribe(ob);
    });
  }

  getLgas(state: any): Observable<any[]> {
    return this.http.get(`${this.API_URL}/state/${state.name}/lga`)
      .map((payload: Object): any[] => {
        return <any[]>payload;
      });
  }

}

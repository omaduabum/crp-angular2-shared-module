import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { HttpInterceptorService } from './http-interceptor.service';
import { LocationService } from './location.service';
import { PaymentService } from './payment.service';

import { CRPService } from './crp.service';
import { CompanyTypeService } from './company-type.service';
import { CompanyClassificationService } from './company-classification.service';
import { NameReservationService } from './name-reservation.service';
import { CompanyService } from './company.service';
import { RegistrationService } from './registration.service';
import { DirectorService } from './director.service';
import { MemberService } from './member.service';
import { ObjectService } from './object.service';
import { ArticleService } from './article.service';
import { AffiliateService } from './affiliate.service';
import { ShareholderService } from './shareholder.service';
import { NatureOfBusinessService } from './nature-of-business.service';
import { TrusteeService } from './trustee.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [
    HttpInterceptorService,
    { provide: HTTP_INTERCEPTORS, useExisting: HttpInterceptorService, multi: true },
    CRPService,
    CompanyTypeService,
    CompanyClassificationService,
    NameReservationService,
    LocationService,
    PaymentService,
    CompanyService,
    RegistrationService,
    DirectorService,
    MemberService,
    ObjectService,
    ArticleService,
    AffiliateService,
    ShareholderService,
    NatureOfBusinessService,
    TrusteeService
  ]
})
export class CrpSharedServicesModule { }

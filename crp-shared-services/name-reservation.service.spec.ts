import { TestBed, inject } from '@angular/core/testing';

import { NameReservationService } from './name-reservation.service';

describe('NameReservationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NameReservationService]
    });
  });

  it('should ...', inject([NameReservationService], (service: NameReservationService) => {
    expect(service).toBeTruthy();
  }));
});

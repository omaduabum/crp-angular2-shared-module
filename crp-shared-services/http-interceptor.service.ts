import { Injectable, EventEmitter, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { AsyncSubject } from 'rxjs/AsyncSubject';

import { AuthenticationService } from '../authentication/authentication.service';

import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  httpError: EventEmitter<HttpErrorResponse> = new EventEmitter();

  authenticationService: AuthenticationService;

  constructor(private router: Router, private injector: Injector) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.authenticationService) {
      this.authenticationService = this.injector.get(AuthenticationService);
    }
    const handled: Observable<HttpEvent<any>> = next.handle(req);
    const subject: AsyncSubject<HttpEvent<any>> = new AsyncSubject();
    handled.subscribe(subject);
    subject.subscribe((event: HttpEvent<any>) => {
      if (event instanceof HttpErrorResponse) {
        if (event.status === 401) {
          this.authenticationService.clearStaleSession();
          return;
        }
        this.httpError.emit(event);
      }
    }, (err: HttpEvent<any>) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          this.authenticationService.clearStaleSession();
          return;
        }
        if (err.status === 404) {
          return;
        }
        this.httpError.emit(err);
      }
    });
    return Observable.create((obs: Observer<HttpEvent<any>>) => {
      subject.subscribe(obs);
    });
  }

}

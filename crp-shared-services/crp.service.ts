import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable, AsyncSubject, Observer } from 'rxjs';

@Injectable()
export class CRPService {

  private nationalities: any[];
  private countries: any[];
  private states: any[];
  private identityTypes: any[];
  private cacBranches: any[];

  constructor(private http: Http, @Inject('API_URL') private API_URL: string) { }

  private fetchArray(path: string, internalObserver: Observer<any[]>): Observable<any[]> {
    return Observable.create((observer: Observer<any[]>) => {
      this.http.get(`${this.API_URL}/${path}`)
        .map((res: Response): any[] => {
          return res.json();
        })
        .subscribe((types: String[]) => {
          internalObserver.next(types);
          observer.next(types);
        }, (error: any) => {
          internalObserver.error(error);
          observer.error(error);
        }, () => {
          internalObserver.complete();
          observer.complete();
        });
    });
  }

  fetchNationalities(): Observable<any[]> {
    if (this.nationalities) {
      return Observable.of(this.nationalities);
    }
    const subject: AsyncSubject<any[]> = new AsyncSubject();
    subject.subscribe((data: any[]) => {
      this.nationalities = data;
    });
    return this.fetchArray('nationality', subject);
  }

  fetchCountries(): Observable<any[]> {
    if (this.countries) {
      return Observable.of(this.countries);
    }
    const subject: AsyncSubject<any[]> = new AsyncSubject();
    subject.subscribe((data: any[]) => {
      this.countries = data;
    });
    return this.fetchArray('country', subject);
  }

  fetchStates(): Observable<any[]> {
    if (this.states) {
      return Observable.of(this.states);
    }
    const subject: AsyncSubject<any[]> = new AsyncSubject();
    subject.subscribe((data: any[]) => {
      this.states = data;
    });
    return this.fetchArray('state', subject);
  }

  fetchIdentityTypes(): Observable<any[]> {
    if (this.identityTypes) {
      return Observable.of(this.identityTypes);
    }
    const subject: AsyncSubject<any[]> = new AsyncSubject();
    subject.subscribe((data: any[]) => {
      this.identityTypes = data;
    });
    return this.fetchArray('identity-type', subject);
  }

  fetchCacBranches(): Observable<any[]> {
    if (this.cacBranches) {
      return Observable.of(this.cacBranches);
    }
    const subject: AsyncSubject<any[]> = new AsyncSubject();
    subject.subscribe((data: any[]) => {
      this.cacBranches = data;
    });
    return this.fetchArray('cac-branch', subject);
  }

}

import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';

@Injectable()
export class CompanyService {

  constructor(private http: HttpClient, @Inject('API_URL') private API_URL: string) { }

  findById(id: number): Observable<any> {
    return this.http.get(`${this.API_URL}/company/${id}`);
  }

}

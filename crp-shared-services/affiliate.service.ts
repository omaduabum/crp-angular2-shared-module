import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';

@Injectable()
export class AffiliateService {

  constructor( @Inject('API_URL') private API_URL: string, private http: HttpClient) { }

  findById(id: number): Observable<Object> {
    return this.http.get(`${this.API_URL}/affiliate/${id}`);
  }

}

import { TestBed, inject } from '@angular/core/testing';

import { ShareholderService } from './shareholder.service';

describe('ShareholderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShareholderService]
    });
  });

  it('should be created', inject([ShareholderService], (service: ShareholderService) => {
    expect(service).toBeTruthy();
  }));
});

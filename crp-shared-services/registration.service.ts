import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';

import { SearchResponse } from '../util/search/SearchResponse.model';

@Injectable()
export class RegistrationService {

  constructor(private http: HttpClient, @Inject('API_URL') private API_URL: string) { }

  getSummary(): Observable<any> {
    return this.http.get(`${this.API_URL}/registration/summary`);
  }

  getRegistrations(filter: any, startIndex: number, length: number): Observable<SearchResponse> {
    filter.start = startIndex;
    filter.length = length;
    return this.http.post(`${this.API_URL}/registration/search`, filter)
      .map((res: Object): SearchResponse => {
        return new SearchResponse(res);
      });
  }

  getStatus(avCode: string): Observable<any> {
    return this.http.get(`${this.API_URL}/availability-code/status?avCode=${avCode}`);
  }

  findRegistration(avCode: string): Observable<any> {
    return this.http.get(`${this.API_URL}/registration/${avCode}`);
  }

  getCompany(avCode: string): Observable<any> {
    return this.http.get(`${this.API_URL}/registration/${avCode}/company`);
  }

  getStampDutyCertificate(avCode: string): Observable<any> {
    return this.http.get(`${this.API_URL}/registration/${avCode}/stamp-duty-certificate`);
  }

  discardRegistration(avCode: string): Observable<any> {
    return this.http.delete(`${this.API_URL}/registration/${avCode}`, { observe: 'response' });
  }

  setAccreditedPresenter(company: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${company.availabilityCode}/presenter/accredited`, {});
  }

  setPresenter(company: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${company.availabilityCode}/presenter`, data);
  }

  updatePresenter(reg: any, data: any): Observable<any> {
    return this.http.put(`${this.API_URL}/registration/${reg.company.availabilityCode}/presenter`, data);
  }

  getCtcTypesForBN(): Observable<any> {
    return this.http.get(`${this.API_URL}/registration/ctc-types/bn`);
  }

  getCtcTypesForLLC(): Observable<any> {
    return this.http.get(`${this.API_URL}/registration/ctc-types/llc`);
  }

  getCtcTypesForIT(): Observable<any> {
    return this.http.get(`${this.API_URL}/registration/ctc-types/it`);
  }

  setCtcApplications(reg: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${reg.company.availabilityCode}/document-details`, data);
  }

  setBusinessNameDetails(reg: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${reg.company.availabilityCode}/company-details/business-name`, data);
  }

  setCompanyDetailsForLtdGte(reg: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${reg.company.availabilityCode}/company-details/limited-by-guarantee`, data);
  }

  setCompanyDetailsForLtdByShares(reg: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${reg.company.availabilityCode}/company-details/limited-by-shares`, data);
  }

  setItDetails(reg: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${reg.company.availabilityCode}/company-details/incorporated-trustee`, data);
  }

  addNob(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/nature-of-business`, data);
  }

  updateNob(registration: any, objectId: number, data: any): Observable<any> {
    return this.http.put(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/nature-of-business/${objectId}`, data);
  }

  removeNob(registration: any, object: any): Observable<any> {
    return this.http.delete(`${this.API_URL}/registration/${
      registration.company.availabilityCode
      }/nature-of-business/${object.id}`);
  }

  addDirector(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/director`, data);
  }

  updateDirector(registration: any, directorId: number, data: any): Observable<any> {
    return this.http.put(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/director/${directorId}`, data);
  }

  addIndividualShareholder(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/shareholder/individual`, data);
  }

  addCorporateShareholder(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/shareholder/corporate`, data);
  }

  updateIndividualShareholder(registration: any, memberId: number, data: any): Observable<any> {
    return this.http.put(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/shareholder/individual/${memberId}`, data);
  }

  updateCorporateShareholder(registration: any, memberId: number, data: any): Observable<any> {
    return this.http.put(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/shareholder/corporate/${memberId}`, data);
  }

  addIndividualMember(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/member/individual`, data);
  }

  addCorporateMember(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/member/corporate`, data);
  }

  updateIndividualMember(registration: any, memberId: number, data: any): Observable<any> {
    return this.http.put(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/member/individual/${memberId}`, data);
  }

  updateCorporateMember(registration: any, memberId: number, data: any): Observable<any> {
    return this.http.put(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/member/corporate/${memberId}`, data);
  }

  addObject(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/object`, data);
  }

  updateObject(registration: any, objectId: number, data: any): Observable<any> {
    return this.http.put(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/object/${objectId}`, data);
  }

  removeObject(registration: any, object: any): Observable<any> {
    return this.http.delete(`${this.API_URL}/registration/${
      registration.company.availabilityCode
      }/object/${object.id}`);
  }

  setIndividualSecretary(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${
      registration.company.availabilityCode
      }/secretary/individual`, data);
  }

  setCorporateSecretary(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${
      registration.company.availabilityCode
      }/secretary/corporate`, data);
  }

  setDeponent(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${
      registration.company.availabilityCode
      }/deponent`, data);
  }

  removeAffiliate(registration: any, affiliate: any): Observable<any> {
    return this.http.delete(`${this.API_URL}/registration/${
      registration.company.availabilityCode
      }/affiliate/${affiliate.id}`);
  }

  getArticleContents(registration: any, articleId: number): Observable<any> {
    return this.http.get(`${this.API_URL}/registration/${
      registration.company.availabilityCode
      }/article/${articleId}/content`);
  }

  addTrustee(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/trustee`, data);
  }

  updateTrustee(registration: any, trusteeId: number, data: any): Observable<any> {
    return this.http.put(`${this.API_URL}/registration/${registration.company.availabilityCode
      }/trustee/${trusteeId}`, data);
  }

  setTrusteeSecretary(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${
      registration.company.availabilityCode
      }/trustee-secretary`, data);
  }

  setTrusteeConstitutionElements(registration: any, data: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${
      registration.company.availabilityCode
      }/trustee-constitution-elements`, data);
  }

  getPayments(company: any, startIndex: number, length: number): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${
      company.availabilityCode
      }/payment/search`,
      { start: startIndex, length: length });
  }

  createSubmissionInvoice(company: any): Observable<any> {
    return this.http.post(`${this.API_URL}/registration/${
      company.availabilityCode
      }/payment`, {});
  }

}

import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable, AsyncSubject, Observer } from 'rxjs'

import { CompanyType } from '../model/company-type.model';
import { CompanyClassification } from '../model/company-classification.model';

import { CompanyTypeService } from './company-type.service';

@Injectable()
export class CompanyClassificationService {

  private companyClasifications: AsyncSubject<CompanyClassification[]>;

  constructor(private companyTypeService: CompanyTypeService) { }

  findById(id: number): Observable<CompanyClassification> {
    return this.fetchAll()
      .flatMap((it: CompanyClassification[]): Observable<CompanyClassification> => Observable.from(it))
      .filter((it: CompanyClassification): boolean => it.id == id)
      .take(1);
  }

  fetchAll(): Observable<CompanyClassification[]> {
    if (!this.companyClasifications) {
      this.companyTypeService.fetchAll()
        .flatMap((types: CompanyType[]): Observable<CompanyType> => Observable.from(types))
        .map((it: CompanyType): CompanyClassification => it.classification)
        .distinct((it: CompanyClassification) => it.id)
        .toArray().subscribe(this.companyClasifications = new AsyncSubject());
    }
    return Observable.create((observer: Observer<CompanyClassification[]>) => {
      this.companyClasifications.subscribe(observer);
    });
  }

}

import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, AsyncSubject, Observer} from 'rxjs';
import {PaymentChannelModel} from '../model/payment-channel-model';

@Injectable()
export class PaymentService {

  constructor(@Inject('API_URL') private API_URL: string, private http: HttpClient) {
  }

  getPayment(id: number): Observable<any> {
    return this.http.get(`${this.API_URL}/payment/${id}`);
  }

  getPaymentInvoice(id: number): Observable<any> {
    return this.http.get(`${this.API_URL}/payment/${id}/invoice`);
  }

  getOasispayTicket(id: number, responseUrl: string): Observable<any> {
    return this.http.post(`${this.API_URL}/payment/${id}/oasispay-ticket`, {responseUrl});
  }

  getRemitaGatewayTicket(id: number, responseUrl: string): Observable<any> {
    return this.http.post(`${this.API_URL}/payment/${id}/oasispay-ticket`, {responseUrl, channelOfPayment: 'REMITA'});
  }

  getPaymentChannelPayTicket(id: number, paymentChannelModel:  PaymentChannelModel ): Observable<any> {
    return this.http.post(`${this.API_URL}/payment/${id}/oasispay-ticket`, {
      responseUrl: paymentChannelModel.responseUrl, channelOfPayment: paymentChannelModel.channelOfPayment});
  }

  getPaymentItems(id: number): Observable<any> {
    return this.http.get(`${this.API_URL}/payment/${id}/item`);
  }
}

import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, AsyncSubject, Observer } from 'rxjs';

import { SearchResponse } from '../util/search/SearchResponse.model';

@Injectable()
export class NatureOfBusinessService {

  private categories: AsyncSubject<any[]>;

  private natureOfBusiness: AsyncSubject<any[]>;

  constructor( @Inject('API_URL') private API_URL: string, private http: HttpClient) { }

  fetchAll(): Observable<any[]> {
    if (!this.natureOfBusiness) {
      this.http.get(`${this.API_URL}/nature-of-business`)
        .subscribe(this.natureOfBusiness = new AsyncSubject());
    }
    return Observable.create((observer: Observer<any[]>) => {
      this.natureOfBusiness.subscribe(observer);
    });
  }

  fetchAllCategories(): Observable<any[]> {
    if (!this.categories) {
      this.http.get(`${this.API_URL}/nature-of-business-category`)
        .subscribe(this.categories = new AsyncSubject());
    }
    return Observable.create((observer: Observer<any[]>) => {
      this.categories.subscribe(observer);
    });
  }

  get(registration: any, start: number, maxResult: number): Observable<Object> {
    return this.http.post(`${this.API_URL}/registration/${
      registration.company.availabilityCode
      }/nature-of-business/search`, { start, length: maxResult })
      .map((obj: any) => new SearchResponse(obj));
  }

}

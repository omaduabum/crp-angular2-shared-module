import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable, AsyncSubject, Observer } from 'rxjs'

import { CompanyType } from '../model/company-type.model';
import { CompanyClassification } from '../model/company-classification.model';

@Injectable()
export class CompanyTypeService {

  private companyTypes: AsyncSubject<CompanyType[]>;

  constructor(private http: Http, @Inject('API_URL') private API_URL: string) { }

  findById(id: number): Observable<CompanyType> {
    return this.fetchAll()
      .flatMap((it: CompanyType[]): Observable<CompanyType> => Observable.from(it))
      .filter((it: CompanyType): boolean => it.id == id)
      .take(1);
  }

  fetchAll(): Observable<CompanyType[]> {
    if (!this.companyTypes) {
      this.companyTypes = new AsyncSubject();

      this.http.get(`${this.API_URL}/company-type`)
        .map((res: Response): Object[] => {
          return res.json();
        })
        .flatMap((types: Object[]): Observable<Object> => Observable.from(types))
        .map((item: Object): CompanyType => {
          return CompanyType.fromObject(item);
        }).toArray()
        .subscribe(this.companyTypes);
    }

    return Observable.create((observer: Observer<CompanyType[]>) => {
      this.companyTypes.subscribe(observer);
    });
  }

}

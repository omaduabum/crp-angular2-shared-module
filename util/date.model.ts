export class Date {

    year: number;
    month: number;
    day: number;

    constructor() {

    }

    toString(): string {
        return `${this.year}-${this.month}-${this.day}`;
    }

    static fromObject(obj: Object): Date {
        const date: Date = new Date();
        date.year = obj["year"];
        date.month = obj["month"];
        date.day = obj["day"];
        return date;
    }
}
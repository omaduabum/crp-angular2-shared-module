import { Directive, ContentChildren, ElementRef, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
  selector: '[checkbox-group]',
  exportAs: 'group',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxGroupDirective),
      multi: true
    }
  ]
})
export class CheckboxGroupDirective implements ControlValueAccessor {

  @Input() compareWith: any;
  private values: any[];
  private valueChanged: any;

  constructor(private el: ElementRef) {
  }

  writeValue(obj: any): void {
    this.values = obj || [];
  }

  registerOnChange(fn: any) {
    this.valueChanged = fn;
  }

  registerOnTouched() {

  }

  toggle(value: any, checked: boolean) {
    if (!this.values) {
      this.values = [];
    }
    const index = this.findIndex(value);
    if (index >= 0) {
      this.values.splice(index, 1);
    } else {
      this.values.push(value);
    }
    this.valueChanged(this.values);
  }

  checked(value: any): boolean {
    return value && this.findIndex(value) >= 0;
  }

  private findIndex(value: any): number {
    return this.values.findIndex((item: any, index: number): boolean => {
      return this.compareWith ? this.compareWith(item, value) : item == value;
    });
  }
}

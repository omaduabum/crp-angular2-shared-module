export class Pager {
    //totalItems: Number;
    //pageSize: Number;
    totalPages: number;

    startPage: number;
    endPage: number;

    //startIndex: Number;
    //endIndex: Number;

    currentPage: number;

    pages(): Array<number> {
        const pages: Array<number> = [];
        for (let i: number = this.startPage; i <= this.endPage; i++) {
            pages.push(i);
        }
        return pages;
    }

    isCurrent(index: number): boolean {
        return this.currentPage === index;
    }

    isFirstPage(): boolean {
        return this.currentPage === 1;
    }

    isLastPage(): boolean {
        return this.currentPage === this.totalPages;
    }
}
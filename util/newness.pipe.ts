import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'newness'
})
export class NewnessPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) {
      return 'N/A';
    }
    let then = moment(value);
    if (then.isAfter(moment())) {
      then = moment();
    }
    return then.fromNow();
  }

}

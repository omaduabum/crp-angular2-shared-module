import * as moment from 'moment';
import { Moment } from 'moment';

export class DateUtils {

    static DATE_PATTERN = /^\d{4}-\d{1,2}-\d{1,2}$/;

    static value(value: string): Moment {
        return moment(value, 'YYYY-MM-DD');
    }

    static isValid(value: string) {
        return moment(value, 'YYYY-MM-DD').isValid();
    }

    static yearsAgo(value: number): Moment {
        return moment().subtract(value, 'years');
    }

    static eighteenOrOlder(value: string) {
        return moment(value, 'YYYY-MM-DD').isSameOrAfter(DateUtils.yearsAgo(18));
    }

    static epochMilliToString(value: number): string {
        return value && moment(value).format('YYYY-MM-DD');
    }

    static isPastOrPresent(value: string): boolean {
        return moment(value, 'YYYY-MM-DD').isSameOrBefore(moment.now());
    }
}

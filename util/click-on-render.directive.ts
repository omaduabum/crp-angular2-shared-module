import { Directive, ElementRef, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appClickOnRender]'
})
export class ClickOnRenderDirective implements AfterViewInit {

  constructor(private elementRef: ElementRef) {
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.click();
  }

}

export class DigitsToWord {

    private static _oneToNineteen = [' ', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
    private static _tens = [' ', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
    private static _powers = ['', 'hundred', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion'];

    public static spell(num: number | string) {
        let arrayOfNums: number[] = this.toArray(num);
        if (!arrayOfNums.length) {
            return '';
        }
        // console.log(arrayOfNums);
        let segments: any[] = DigitsToWord.toSegments(arrayOfNums, 3);
        // console.log(segments);

        let segmentSpellings: string[] = [];
        for (let i = 0; i < segments.length; i++) {
            let spelling = DigitsToWord.spellSegment(segments[i]);
            if (!spelling) {
                continue;
            }
            // console.log(segments[i], spelling);
            if (segments.length - i > 1) {
                spelling += ' ' + DigitsToWord._powers[segments.length - i];
            }
            segmentSpellings.push(spelling);
        }
        const lastSegmentSpelling = segmentSpellings.pop();
        let spelling = segmentSpellings.join(', ');
        if (!spelling) {
            spelling = lastSegmentSpelling;
        }
        else if (lastSegmentSpelling) {
            if (lastSegmentSpelling.indexOf('and') >= 0) {
                spelling += ', ' + lastSegmentSpelling;
            } else {
                spelling += ' and ' + lastSegmentSpelling;
            }
        }
        return spelling;
    }

    private static toArray(input): number[] {
        let stringNum;
        if (typeof (input) == 'string') {
            stringNum = input;
        }
        else {
            stringNum = input.toString();
        }
        let arrayOfNums = stringNum.split('.')[0].split('');
        let length = arrayOfNums.length;
        for (let i = 0; i < length; i++) { arrayOfNums[i] = +arrayOfNums[i]; }
        return arrayOfNums;
    }

    private static toSegments(items: any[], segmentWidth: number): any[] {
        let length = items.length;
        let segments = [];
        while (length > 0) {
            segments.push(items.splice(segmentWidth * -1, segmentWidth));
            length = items.length;
        }
        return segments.reverse();
    }

    private static spellSegment(segment: number[]): string {
        let word = '';
        while (segment.length < 3) {
            segment = [0].concat(segment);
        }
        if (segment[0] > 0) {
            word = DigitsToWord._oneToNineteen[segment[0]] + ' hundred';
        }
        if (!segment[1] && segment[2]) {
            if (word) {
                word += ' and ' + DigitsToWord._oneToNineteen[segment[2]];
            } else {
                word = DigitsToWord._oneToNineteen[segment[2]];
            }
        }
        else if (segment[1] === 1) {
            const val = (segment[1] * 10) + segment[2];
            if (word) {
                word += ' and ' + DigitsToWord._oneToNineteen[val];
            } else {
                word = DigitsToWord._oneToNineteen[val];
            }
        } else if (segment[1] > 1) {
            if (word) {
                word += ' and ' + DigitsToWord._tens[segment[1]];
            } else {
                word = DigitsToWord._tens[segment[1]];
            }
            if (segment[2]) {
                word += '-' + DigitsToWord._oneToNineteen[segment[2]];
            }
        }
        return word;
    }

}
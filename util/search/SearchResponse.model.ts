export class SearchResponse {

    draw: number;
    start: number;
    recordsTotal: number;
    recordsFiltered: number;
    data: Array<any>;
    rawData: any;

    constructor(response: Object) {
        this.draw = response['draw'];
        this.start = response['start'];
        this.recordsTotal = response['recordsTotal'];
        this.recordsFiltered = response['recordsFiltered'];
        this.data = response['data'];
        this.rawData = response;
    }

    hasMore(): boolean {
        return this.data.length < this.recordsFiltered;
    }
}
import { Component, Inject, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pager } from '../pager.model';
import { PagerService } from '../pager.service';
import { SearchResponse } from '../search/SearchResponse.model';

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css']
})
export class PagerComponent implements OnInit {

  pager: Pager;
  @Input() itemsPerPage: number;
  @Output() showFromIndex: EventEmitter<number>;

  constructor(private pagerService: PagerService) {
    this.showFromIndex = new EventEmitter<number>();
  }

  ngOnInit() {
  }

  @Input('searchResponse')
  set searchResponse(res: SearchResponse) {
    // console.log("new search response");
    if (!res) {
      this.pager = null;
    } else {
      this.pager = this.pagerService.getPager(res.start, res.recordsFiltered, this.itemsPerPage);
      // console.log(JSON.stringify(this.pager));
    }
  }

  hideOnSmallScreen(index: number): boolean {
    const currentPage = this.pager.currentPage;
    if (index === currentPage) {
      return false;
    }
    if (currentPage < 4 && index < 5) {
      return false;
    }
    if (Math.abs(index - currentPage) === 1 || currentPage - index === 2) {
      return false;
    }
    if (this.pager.isLastPage()) {
      return currentPage - index > 3;
    }
    return true;
  }

  requestPage(index: number): void {
    this.showFromIndex.emit((index - 1) * this.itemsPerPage);
  }

  previous(): void {
    if (!this.pager || this.pager.isFirstPage()) {
      return;
    }
    this.requestPage(this.pager.currentPage - 1);
  }

  next(): void {
    if (!this.pager || this.pager.isLastPage()) {
      return;
    }
    this.requestPage(this.pager.currentPage + 1);
  }

}

import { Directive, ElementRef, AfterViewInit, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appOnRender]'
})
export class OnRenderDirective implements AfterViewInit {

  @Output() rendered: EventEmitter<ElementRef> = new EventEmitter();

  constructor(private elementRef: ElementRef) { }

  ngAfterViewInit() {
    this.rendered.emit(this.elementRef);
  }

}

import { Directive, ContentChildren, ElementRef, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
  selector: '[radio-group]',
  exportAs: 'radioGroup',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioGroupDirective),
      multi: true
    }
  ]
})
export class RadioGroupDirective implements ControlValueAccessor {

  @Input() compareWith: any;
  private value: any;
  private valueChanged: any;

  constructor(private el: ElementRef) {
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  registerOnChange(fn: any) {
    this.valueChanged = fn;
  }

  registerOnTouched() {

  }

  select(value: any) {
    if (!this.isSelected(value)) {
      this.value = value;
      this.valueChanged(this.value);
    }
  }

  isSelected(value: any): boolean {
    if (!value || !this.value) {
      return false;
    }
    return this.compareWith ? this.compareWith(this.value, value) : this.value == value;
  }
}

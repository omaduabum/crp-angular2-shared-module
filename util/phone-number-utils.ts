import { parse, format, asYouType, isValidNumber, ParsedNumber } from 'libphonenumber-js/'

export class PhoneNumberUtils {

    static isValid(value: string): boolean {
        if (!value) return true;
        if (value.startsWith('0')) {
            return /^0\d{10}$/.test(value);
        }
        const phoneNumber: ParsedNumber = parse(value);
        // console.log('phoneNumber: ', phoneNumber);
        if (phoneNumber.country && phoneNumber.country.toString() === "NG") {
            // console.log(`0${phoneNumber.phone.toString()}`);
            return /^\d{10}$/.test(phoneNumber.phone.toString());
        }
        return isValidNumber(phoneNumber);
    }

    static countryCode(value: string): string {
        if (!value) return null;
        const phoneNumber: ParsedNumber = parse(value);
        console.log(value, phoneNumber);
        if (phoneNumber.country) {
            return phoneNumber.country.toString();
        }
        return null;
    }
}

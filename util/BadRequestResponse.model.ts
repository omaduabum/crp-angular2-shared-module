export class BadRequestResponse {

    globalErrors: ErrorItem[];
    fieldErrors: FieldErrors[];

    // {"globalErrors":[],"fieldErrors":{"targetCompany":[{"code":"Exists","message":"Target company does not exist"}]}}
    static load(payload: any): BadRequestResponse {
        const brr = new BadRequestResponse();
        if (payload.globalErrors) {
            brr.globalErrors = payload.globalErrors.map(ErrorItem.parse);
        }
        if (payload.fieldErrors) {
            brr.fieldErrors = Object.keys(payload.fieldErrors).map((fieldName: string): FieldErrors => {
                const field: FieldErrors = new FieldErrors();
                field.name = fieldName;
                field.errors = payload.fieldErrors[fieldName].map(ErrorItem.parse);
                return field;
            });
        }
        return brr;
    }

    static parse(json: string): BadRequestResponse {
        return BadRequestResponse.load(JSON.parse(json));
    }

    getAll(): ErrorItem[] {
        let errors: ErrorItem[] = this.globalErrors || [];
        if (this.fieldErrors) {
            this.fieldErrors.forEach((it: FieldErrors) => {
                errors = errors.concat(it.errors);
            });
        }
        return errors;
    }
}

export class FieldErrors {
    name: string;
    errors: ErrorItem[];
}

export class ErrorItem {
    code: string;
    message: string;

    static parse(item: any): ErrorItem {
        const it = new ErrorItem();
        it.code = item.code;
        it.message = item.message;
        return it;
    }
}

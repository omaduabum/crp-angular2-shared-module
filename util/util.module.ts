import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagerComponent } from './pager/pager.component';
import { PagerService } from './pager.service';
import { CheckboxGroupDirective } from './checkbox-group/checkbox-group.directive';
import { RadioGroupDirective } from './radio-group/radio-group.directive';
import { MomentPipe } from './moment.pipe';
import { NewnessPipe } from './newness.pipe';
import { ClickOnRenderDirective } from './click-on-render.directive';
import { OnRenderDirective } from './on-render.directive';
import { NairaPipe } from './naira.pipe';
import { AmountInWordsPipe } from './amount-in-words.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    PagerComponent,
    CheckboxGroupDirective,
    RadioGroupDirective,
    MomentPipe,
    NewnessPipe,
    ClickOnRenderDirective,
    OnRenderDirective,
    NairaPipe
  ],
  declarations: [
    PagerComponent,
    CheckboxGroupDirective,
    RadioGroupDirective,
    MomentPipe,
    NewnessPipe,
    ClickOnRenderDirective,
    OnRenderDirective,
    NairaPipe,
    AmountInWordsPipe
  ],
  providers: [PagerService]
})
export class UtilModule { }

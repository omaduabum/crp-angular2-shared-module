import { Injectable } from '@angular/core';
import { Pager } from './pager.model';

@Injectable()
export class PagerService {

  getPager(start: number = 0, totalItems: number, pageSize: number = 10): Pager {
    const pager: Pager = new Pager();

    // calculate total pages
    pager.totalPages = Math.ceil(totalItems / pageSize);
    pager.currentPage = Math.ceil(start / pageSize) + 1;

    // let startPage: number, endPage: number;
    if (pager.totalPages <= 10) {
      // less than 10 total pages so show all
      pager.startPage = 1;
      pager.endPage = pager.totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (pager.currentPage <= 6) {
        pager.startPage = 1;
        pager.endPage = 10;
      } else if (pager.currentPage + 4 >= pager.totalPages) {
        pager.startPage = pager.totalPages - 9;
        pager.endPage = pager.totalPages;
      } else {
        pager.startPage = pager.currentPage - 5;
        pager.endPage = pager.currentPage + 4;
      }
    }

    return pager;
  }
}

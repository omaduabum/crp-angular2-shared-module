import { TestBed, inject } from '@angular/core/testing';

import { ErrorListenerService } from './error-listener.service';

describe('ErrorListenerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ErrorListenerService]
    });
  });

  it('should be created', inject([ErrorListenerService], (service: ErrorListenerService) => {
    expect(service).toBeTruthy();
  }));
});

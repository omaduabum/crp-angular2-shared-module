import { Injectable, ErrorHandler, Inject, Injector } from '@angular/core';
import { Http, Response } from '@angular/http';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import * as StackTrace from 'stacktrace-js';

@Injectable()
export class ErrorListenerService implements ErrorHandler {

  constructor(
    private injector: Injector,
    @Inject('API_URL') private API_URL: string,
    private http: Http) { }

  handleError(error) {
    try {
      const location = this.injector.get(LocationStrategy);
      const message = error.message ? error.message : error.toString();
      const url = location instanceof PathLocationStrategy
        ? location.path() : '';
      // if (!(error instanceof Error)) {
      //   return;
      // }
      StackTrace.fromError(error).then(stackframes => {
        const stackTrace = stackframes
          .splice(0, 20)
          .map(function (sf) {
            return sf.toString();
          }).join('\n');
        this.http.post(`${this.API_URL}/log/error`, { message, url, stackTrace })
          .subscribe((res: Response) => { });
      });
    } catch (error) {

    }

    throw error;
  }

}

export class WizardStep {

    isFirstStep: boolean;
    isLastStep: boolean;
    prevStep: string;
    nextStep: string;
    canContinue; fn;

    constructor() {

    }
}
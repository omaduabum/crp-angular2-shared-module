import { CompanyClassification } from './company-classification.model';

export class CompanyType {

    id: number;
    name: string;
    description: string;
    classification: CompanyClassification;

    static fromObject(obj: Object): CompanyType {
        const type: CompanyType = new CompanyType();
        type.id = obj['id'];
        type.name = obj['name'] && obj['name'];
        type.description = obj['description'];
        type.description = type.description && type.description.replace(/_/g, ' ');
        if (obj['classification']) {
            type.classification = CompanyClassification.fromObject(obj['classification']);
        }
        return type;
    }

    isLimitedByGuarantee() {
        switch (this.name) {
            case 'LIMITED_BY_GUARANTEE':
            case 'PRIVATE_COMPANY_LIMITED_BY_GUARANTEE':
            case 'PUBLIC_COMPANY_LIMITED_BY_GUARANTEE':
                return true;
            default:
                return false;
        }
    }

    isLimitedByShares() {
        switch (this.name) {
            case 'PRIVATE_COMPANY_LIMITED_BY_SHARES':
            case 'PUBLIC_COMPANY_LIMITED_BY_SHARES':
                return true;
            default:
                return false;
        }
    }

    isUnlimited() {
        switch (this.name) {
            case 'PRIVATE_UNLIMITED_COMPANY':
            case 'PUBLIC_UNLIMITED_COMPANY':
                return true;
            default:
                return false;
        }
    }

    getNameGuide(): string {
        if (this.name === 'BUSINESS_NAME') {
            return 'enter a valid business name';
        } else if (this.isLimitedByGuarantee()) {
            return 'name should end with ltd/gte';
        } else if (this.name === 'PRIVATE_COMPANY_LIMITED_BY_SHARES') {
            return 'name should end with ltd or limited';
        } else if (this.name === 'PUBLIC_COMPANY_LIMITED_BY_SHARES') {
            return 'name should end with plc';
        } else if (this.isUnlimited()) {
            return 'name should end with unlimited';
        }
        return 'enter a valid name';
    }
}

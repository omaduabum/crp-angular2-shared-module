export enum NameReservationReason {
    NEW, CHANGE_OF_NAME, CONVERSION, SUBSIDIARY
}

export class NameReservationReasonHelper {
    static getDescription(reason: NameReservationReason): string {
        switch (reason) {
            case NameReservationReason.NEW: return 'New Incorporation';
            case NameReservationReason.CHANGE_OF_NAME: return 'Change of Name';
            case NameReservationReason.CONVERSION: return 'Conversion';
            case NameReservationReason.SUBSIDIARY: return 'Subsidiary/Affiliate';
            default: return '';
        }
    }

    static getReasonType(description: string): NameReservationReason {
        if (!description) {
            return null;
        }
        switch (description.toLowerCase()) {
            case 'new incorporation': return NameReservationReason.NEW;
            case 'change of name': return NameReservationReason.CHANGE_OF_NAME;
            case 'conversion': return NameReservationReason.CONVERSION;
            case 'subsidiary/affiliate': return NameReservationReason.SUBSIDIARY;
            default: return null;
        }
    }
}

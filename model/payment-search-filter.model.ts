import { Date } from '../util/date.model';

export class PaymentSearchFilter {
    startDate: Date;
    endDate: Date;
    rrr: String;
    reference: String;
}
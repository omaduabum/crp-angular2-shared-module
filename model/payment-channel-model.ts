export class PaymentChannelModel {
  private _responseUrl: String;
  private  _channelOfPayment: String;

  get responseUrl(): String {
    return this._responseUrl;
  }

  set responseUrl(value: String) {
    this._responseUrl = value;
  }


  get channelOfPayment(): String {
    return this._channelOfPayment;
  }

  set channelOfPayment(value: String) {
    this._channelOfPayment = value;
  }
}

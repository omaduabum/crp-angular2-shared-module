export class CompanyClassification {

    id: number;
    name: string;
    description: string;

    static fromObject(obj: Object): CompanyClassification {
        const classification: CompanyClassification = new CompanyClassification();
        classification.id = obj['id'];
        classification.name = obj['name'];
        classification.description = obj['description'];
        classification.description = classification.description.replace(/_/g, ' ');
        return classification;
    }

    isBusinessName() {
        return this.name === 'BUSINESS_NAME';
    }

    isLLC() {
        return this.name === 'COMPANY';
    }

    isIT() {
        return this.name === 'INCORPORATED_TRUSTEE';
    }
}
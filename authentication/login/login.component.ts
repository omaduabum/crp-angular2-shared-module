import { Component, OnInit } from '@angular/core';
import {
  Response
} from '@angular/http';
import {
  FormBuilder,
  FormGroup,
  AbstractControl,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';

import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  username: AbstractControl;
  password: AbstractControl;
  loading: boolean;

  error: string;

  constructor(
    fb: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService) {
    this.loading = false;
    this.form = fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required]
    });
    this.username = this.form.controls['username'];
    this.password = this.form.controls['password'];
  }

  ngOnInit() {
  }

  submit(): void {
    // console.log('submit');
    if (this.loading || this.form.invalid) {
      return;
    }
    this.loading = true;
    const value = this.form.value;
    this.authenticationService.login(value['username'].trim(), value['password'])
      .subscribe((user: any) => {
        // console.log("user: " + JSON.stringify(user));
        this.loading = false;
        const url = this.authenticationService.lastProtectedUrl;
        if (url) {
          this.router.navigateByUrl(url);
          this.authenticationService.lastProtectedUrl = null;
        } else {
          this.router.navigate(['/']);
        }
      }, (res: Response) => {
        this.error = res.status === 401 ? 'Username or Password incorrect' : res.statusText;
        this.loading = false;
      });
  }

}
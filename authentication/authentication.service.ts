import { Injectable, Inject, EventEmitter, OnInit } from '@angular/core';
import {
  Http,
  Response,
  RequestOptions,
  Headers
} from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, Observer, AsyncSubject, BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { environment } from 'app/../environments/environment';

@Injectable()
export class AuthenticationService implements OnInit {

  user: BehaviorSubject<any> = new BehaviorSubject(undefined);
  private _user: Object;
  private ongoingFetch: Observable<any>;
  private initialized: boolean;
  lastProtectedUrl: string;

  constructor(
    private http: Http,
    private httpClient: HttpClient,
    private router: Router) {
    this.user.subscribe((user: any) => {
      // console.log('user set');
      this._user = user;
    });
    this.fetch().subscribe((res => {
      this.initialized = true;
    }), (res => {
      this.initialized = true;
    }));
  }

  ngOnInit() {
  }

  clearStaleSession() {
    this.user.next(null);
    location.href = [environment.crpBaseUrl === '/' ? '' : environment.crpBaseUrl, 'login'].join('/');
  }

  register(data: any): Observable<any> {
    return this.http.post(`${environment.authApiBaseUrl}/user`, data)
      .map((res: Response) => res.json())
      .map((user: any) => {
        // console.log("authorization: " + res.headers.get('authorization'));
        this.user.next(user);
        return user;
      });
  }

  updateUserDetails(data: any): Observable<any> {
    return this.httpClient.post(`${environment.authApiBaseUrl}/user-details`, data)
      .map((user: any) => {
        this.user.next(user);
        return user;
      });
  }

  changePassword(password: string): Observable<any> {
    return this.httpClient.post(`${environment.authApiBaseUrl}/change-password`, { password }, { observe: 'response' });
  }

  login(username: String, password: string): Observable<any> {
    // const options = new RequestOptions();
    // options.withCredentials = true;
    // options.headers.set('Content-Type', 'application/json');
    return this.http.post(`${environment.authApiBaseUrl}/login`, { username, password })
      .map((res: Response) => res.json())
      .map((user: any) => {
        // console.log("authorization: " + res.headers.get('authorization'));
        this.user.next(user);
        return user;
      });
  }

  requestMailVerificationToken(): Observable<any> {
    return this.httpClient.post(`${environment.authApiBaseUrl}/request-verification-mail`, { });
  }

  requestPasswordResetToken(email: string): Observable<any> {
    return this.http.post(`${environment.authApiBaseUrl}/password-reset-request`, { email });
  }

  resetPassword(data: any): Observable<any> {
    return this.http.post(`${environment.authApiBaseUrl}/reset-password`, data)
      .map((user: any) => {
        this.user.next(user);
        return this._user;
      });
  }

  logout(): Observable<Response> {
    const subject: AsyncSubject<Response> = new AsyncSubject();
    this.http.post(`${environment.authApiBaseUrl}/logout`, {})
      .subscribe(subject);
    subject.subscribe((res: any) => {
      this.clearStaleSession();
    });
    return Observable.create((observer: Observer<Response>) => {
      subject.subscribe(observer);
    });
  }

  fetchUser(): Observable<any> {
    if (this.initialized) {
      return Observable.of(this._user);
    }
    return this.fetch();
  }

  private fetch() {
    if (!this.ongoingFetch) {
      console.log('fetching user...', new Date());
      // const options = new RequestOptions();
      // options.withCredentials = true;
      this.ongoingFetch = this.http.get(`${environment.authApiBaseUrl}/me`)
        .map((res: Response) => res.json());

      this.ongoingFetch.subscribe((user: any) => {
        console.log('user fetched', user);
        this.ongoingFetch = null;
        this.user.next(user);
      }, (err: any) => {
        this.user.next(null);
        console.error(err);
      });
    } else {
      console.log('fetching user ongoing...');
    }
    return this.ongoingFetch;
  }

}

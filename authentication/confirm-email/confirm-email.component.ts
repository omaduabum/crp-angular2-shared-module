import { Component, Injectable, Inject, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http, Response } from '@angular/http';
import { environment } from 'app/../environments/environment';

import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.css']
})
export class ConfirmEmailComponent implements OnInit, OnDestroy {

  private routeObserver: any;
  loading = true;
  user: any;
  usedToken: any;
  badToken: boolean;
  errorMessage: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private http: Http,
    @Inject('API_URL') private API_URL: string) {

    authenticationService.user.subscribe((user: any) => { this.user = user; });
    this.routeObserver = this.activatedRoute.params
      .map(params => {
        return params['token'];
      })
      .distinctUntilChanged()
      .subscribe(token => {
        this.loading = true;
        this.badToken = false;
        this.usedToken = null;
        this.errorMessage = null;
        this.http.post(`${environment.authApiBaseUrl}/verify-email`, { token })
          .subscribe((res: Response) => {
            this.loading = false;
            this.usedToken = token;
            if (this.user && this.user.portalUser) {
              this.user.portalUser.emailAddressVerified = true;
            }
            // console.log(res);
          }, (err: Response) => {
            this.loading = false;
            this.usedToken = token;
            this.badToken = err.status === 400;
            this.errorMessage = err.statusText;
            // console.log(err);
          });
      });
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.routeObserver.unsubscribe();
  }

}

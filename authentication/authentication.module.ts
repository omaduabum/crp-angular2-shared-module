import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgxErrorsModule } from '@ultimate/ngxerrors';

import { LoginComponent } from './login/login.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { AuthenticationService } from './authentication.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import {NotLoggedInGuard} from "./not-logged-in.guard";
import {LoggedInGuard} from "./logged-in.guard";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule,
    NgxErrorsModule
  ],
  exports: [
    LoginComponent,
    ConfirmEmailComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent
  ],
  declarations: [
    LoginComponent,
    ConfirmEmailComponent,
    ChangePasswordComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent
  ],
  providers: [AuthenticationService, LoggedInGuard, NotLoggedInGuard]
})
export class AuthenticationModule { }

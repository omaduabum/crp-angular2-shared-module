import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { environment } from 'app/../environments/environment';
import {AuthenticationService} from "./authentication.service";

@Injectable()
export class LoggedInGuard implements CanActivate {

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.authenticationService.fetchUser()
      .map((user => {
        if (!user) {
          this.authenticationService.lastProtectedUrl = state.url;
          location.href = [this.getCrpBaseUrl(), 'login'].join('/');
        }
        return !!user;
      }))
      .catch((err: any, caught: Observable<any>) => {
        this.authenticationService.lastProtectedUrl = state.url;
        location.href = [this.getCrpBaseUrl(), 'login'].join('/');
        return Observable.of(false);
      });
  }

  private getCrpBaseUrl() {
    if (environment.crpBaseUrl === '/') {
      return '';
    }
    return environment.crpBaseUrl;
  }
}

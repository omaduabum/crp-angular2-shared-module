import { Pipe, PipeTransform } from '@angular/core';

import { environment } from 'app/../environments/environment';

@Pipe({
  name: 'allFormsDownloadUrl'
})
export class AllFormsDownloadUrlPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return `${environment.documentApiBaseUrl}/company/${value.id}/registration`;
  }

}

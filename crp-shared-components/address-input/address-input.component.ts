import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  forwardRef
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  NG_VALUE_ACCESSOR,
  Validators,
  ControlValueAccessor,
  AbstractControl
} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import {CRPService} from "../../crp-shared-services/crp.service";


@Component({
  selector: 'app-address-input',
  templateUrl: './address-input.component.html',
  styleUrls: ['./address-input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AddressInputComponent),
      multi: true
    }
  ]
})
export class AddressInputComponent implements OnInit {

  form: FormGroup;

  loading = true;
  countries: any[];
  nigeria: any;
  states: any[];

  constructor(private fb: FormBuilder, private crpService: CRPService) {
    this.form = fb.group({
      country: ['', Validators.required],
      state: ['', this.requiresState()],
      city: ['', Validators.compose([
        Validators.required, Validators.maxLength(255)
      ])],
      address: ['', Validators.compose([
        Validators.required, Validators.maxLength(255)
      ])]
    });
    crpService.fetchStates()
      .subscribe((states: any[]) => {
        this.states = states;
        this.loading = false;
      });
  }

  ngOnInit() {
    this.form.controls['country'].valueChanges.subscribe(() => {
      this.form.controls['state'].updateValueAndValidity();
    });
    Observable.merge(
      this.crpService.fetchCountries()
        .map((data: any[]) => ['countries', data]),
      this.crpService.fetchStates()
        .map((data: any[]) => ['states', data])
    )
      .reduce((out: any, data: any[]): any => {
        out[data[0]] = data[1];
        return out;
      }, {})
      .subscribe((data: any) => {
        this.countries = data['countries'];
        this.nigeria = this.countries
          .filter((it: any) => it['name'].toLowerCase() === 'nigeria')[0];
        if (!this.form.controls['country'].value) {
          this.form.controls['country'].setValue(this.nigeria);
        }
        this.states = data['states'];
      });
  }

  writeValue(obj: any): void {
    // console.log(obj);
    this.form.patchValue(obj || {});
    if (!this.form.controls['country']) {
      this.form.controls['country'].setValue(this.nigeria);
    }
  }

  registerOnChange(fn: any): void {
    this.refreshValue(fn);
    this.form.valueChanges
      .subscribe((data: any) => {
        this.refreshValue(fn);
      });
  }

  private refreshValue(fn: any) {
    if (this.form.invalid) {
      fn(null);
    } else {
      const value = JSON.parse(JSON.stringify(this.form.value));
      value.country = value.country && value.country.name;
      if (this.isResidentInNigeria()) {
        value.state = value.state && value.state.name;
      } else {
        value.state = undefined;
      }
      fn(value);
    }
  }

  registerOnTouched(fn: any): void { }

  setDisabledState?(isDisabled: boolean): void { }

  isInvalid(field: string): boolean {
    const control: AbstractControl = this.form.controls[field];
    return control.touched && control.invalid;
  }

  requiresState() {
    const self = this;
    return (control: AbstractControl) => {
      if (!self.form || !self.isResidentInNigeria()) {
        return null;
      }
      return (!control.value || !control.value.name) ? { required: true } : null;
    };
  }

  isResidentInNigeria() {
    const countryOfResidence = this.form.controls['country'].value;
    return countryOfResidence && countryOfResidence.name && countryOfResidence.name.toLowerCase() === 'nigeria';
  }

  equals(a: any, b: any): boolean {
    if (!a && !b) {
      return true;
    }
    if (!a || !b || !a.name || !b.name) {
      return false;
    }
    return a.name.toLowerCase() === b.name.toLowerCase();
  }

}

import { Pipe, PipeTransform } from '@angular/core';

import { environment } from 'app/../environments/environment';

@Pipe({
  name: 'applicationFormDownloadUrl'
})
export class ApplicationFormDownloadUrlPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return `${environment.documentApiBaseUrl}/company/${value.id}/registration/application`;
  }

}

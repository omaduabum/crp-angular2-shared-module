import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../authentication/authentication.service";

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.css']
})
export class EmailVerificationComponent implements OnInit {

  user: any;

  sendingVerificationMail: boolean;
  verificationMailSent: boolean;

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.user
      .subscribe((user: any) => {
        if (!user) {
          return;
        }
        this.user = user.portalUser;
      });
  }

  resendVerificationMail(): boolean {
    this.verificationMailSent = false;
    this.sendingVerificationMail = true;
    this.authenticationService.requestMailVerificationToken()
      .delay(1000)
      .subscribe(() => {
        this.sendingVerificationMail = false;
        this.verificationMailSent = true;
      }, () => {
        this.sendingVerificationMail = false;
      });
    return false;
  }

}

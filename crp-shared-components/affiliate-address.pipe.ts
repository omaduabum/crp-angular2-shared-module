import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'affiliateAddress'
})
export class AffiliateAddressPipe implements PipeTransform {

  transform(affiliate: any, args?: any): any {
    let address: string = affiliate.address;
    if (affiliate.state) {
      address += ` (${affiliate.city}, ${affiliate.state})`;
    }
    return address;
  }

}

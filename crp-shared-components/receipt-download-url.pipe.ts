import { Pipe, PipeTransform } from '@angular/core';

import { environment } from 'app/../environments/environment';

@Pipe({
  name: 'receiptDownloadUrl'
})
export class ReceiptDownloadUrlPipe implements PipeTransform {

  constructor() { }

  transform(value: any, args?: any): any {
    return `${environment.documentApiBaseUrl}/payment/${value.id}/receipt`;
  }

}

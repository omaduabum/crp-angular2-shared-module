import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { UtilModule } from '../util/util.module';

import { AddressPreviewComponent } from './address-preview/address-preview.component';
import { PaymentItemComponent } from './payment-item/payment-item.component';
import { IdentityInputComponent } from './identity-input/identity-input.component';
import { AddressInputComponent } from './address-input/address-input.component';
import { ContactDetailsInputComponent } from './contact-details-input/contact-details-input.component';
import { InvoicePaymentComponent } from './invoice-payment/invoice-payment.component';
import { AffiliateNamePipe } from './affiliate-name.pipe';
import { AffiliateAddressPipe } from './affiliate-address.pipe';
import { ReceiptDownloadUrlPipe } from './receipt-download-url.pipe';
import { ApplicationFormDownloadUrlPipe } from './application-form-download-url.pipe';
import { TrusteeConstitutionFormDownloadUrlPipe } from './trustee-constitution-form-download-url.pipe';
import { TrusteeDeclarationFormDownloadUrlPipe } from './trustee-declaration-form-download-url.pipe';
import { AllFormsDownloadUrlPipe } from './all-forms-download-url.pipe';
import { MemartFormDownloadUrlPipe } from './memart-form-download-url.pipe';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { CompanyAddressPipe } from './company-address.pipe';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    UtilModule,
    NgxErrorsModule
  ],
  exports: [
    AddressPreviewComponent,
    PaymentItemComponent,
    IdentityInputComponent,
    AddressInputComponent,
    ContactDetailsInputComponent,
    InvoicePaymentComponent,
    EmailVerificationComponent,
    AffiliateNamePipe,
    AffiliateAddressPipe,
    ReceiptDownloadUrlPipe,
    ApplicationFormDownloadUrlPipe,
    TrusteeDeclarationFormDownloadUrlPipe,
    TrusteeConstitutionFormDownloadUrlPipe,
    AllFormsDownloadUrlPipe,
    MemartFormDownloadUrlPipe,
    CompanyAddressPipe
  ],
  declarations: [
    AddressPreviewComponent,
    PaymentItemComponent,
    IdentityInputComponent,
    AddressInputComponent,
    ContactDetailsInputComponent,
    InvoicePaymentComponent,
    AffiliateNamePipe,
    AffiliateAddressPipe,
    ReceiptDownloadUrlPipe,
    ApplicationFormDownloadUrlPipe,
    TrusteeConstitutionFormDownloadUrlPipe,
    TrusteeDeclarationFormDownloadUrlPipe,
    AllFormsDownloadUrlPipe,
    MemartFormDownloadUrlPipe,
    EmailVerificationComponent,
    CompanyAddressPipe
  ]
})
export class CrpSharedComponentsModule { }

import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  forwardRef
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  NG_VALUE_ACCESSOR,
  Validators,
  ControlValueAccessor,
  AbstractControl
} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import {PhoneNumberUtils} from "../../util/phone-number-utils";

@Component({
  selector: 'app-contact-details-input',
  templateUrl: './contact-details-input.component.html',
  styleUrls: ['./contact-details-input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ContactDetailsInputComponent),
      multi: true
    }
  ]
})
export class ContactDetailsInputComponent implements OnInit, ControlValueAccessor {

  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'),
        Validators.maxLength(255)
      ])],
      phoneNumber: ['', Validators.compose([
        Validators.required,
        // Validators.pattern(/^(\+\d{1,3}[- ]?|0)\d{10}$/),
        (control: AbstractControl) => {
          return PhoneNumberUtils.isValid(control.value) ? null : { pattern: true };
        },
        Validators.maxLength(255)
      ])]
    });
  }

  writeValue(obj: any): void {
    this.form.patchValue(obj || {});
  }

  registerOnChange(fn: any): void {
    this.form.valueChanges.subscribe((data: any) => {
      this.updateValue(fn);
    });
    this.updateValue(fn);
  }

  private updateValue(fn: any) {
    if (this.form.invalid) {
      fn(null);
    } else {
      fn(this.form.value);
    }
  }

  registerOnTouched(fn: any): void { }

  setDisabledState?(isDisabled: boolean): void { }

  isInvalid(field: string): boolean {
    const control: AbstractControl = this.form.controls[field];
    return control.touched && control.invalid;
  }

}

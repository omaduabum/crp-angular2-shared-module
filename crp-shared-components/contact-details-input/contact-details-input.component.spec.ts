import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactDetailsInputComponent } from './contact-details-input.component';

describe('ContactDetailsInputComponent', () => {
  let component: ContactDetailsInputComponent;
  let fixture: ComponentFixture<ContactDetailsInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactDetailsInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactDetailsInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

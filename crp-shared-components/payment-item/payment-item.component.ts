import { Component, OnInit, Input } from '@angular/core';
import {PaymentService} from "../../crp-shared-services/payment.service";


@Component({
    selector: '[app-payment-item]',
    templateUrl: './payment-item.component.html',
    styleUrls: ['./payment-item.component.css']
})
export class PaymentItemComponent implements OnInit {

    @Input() payment: any;
    working: boolean;
    isRemita: boolean;
    isNibss: boolean;

    constructor(private paymentService: PaymentService) { }

    ngOnInit() {
        // console.log(this.payment.channel);
        this.checkIfisRemita();
        this.checkIfisNibbs();
    }

    checkIfisRemita() {
        if (this.payment.channel === "remita") {
            this.isRemita = true;
        }else {
            this.isRemita = false;
        }
    }

    checkIfisNibbs() {
        if (this.payment.channel === "nibss") {
            this.isNibss = true;
        }else {
            this.isNibss = false;
        }
    }

    isPending(): boolean {
        return this.payment.paymentStatus === 'PENDING';
    }

    isSuccessful(): boolean {
        return this.payment.paymentStatus === 'APPROVED';
    }

    lastUpdated() {
        return this.payment.ebillsPayTransactionCompletionDate || this.payment.paymentDate;
    }

    refresh(): void {
        if (this.working) {
            return;
        }
        this.working = true;
        this.paymentService.getPaymentInvoice(this.payment.id)
            .delay(1000)
            .subscribe((payment: any) => {

                Object.keys(payment).forEach((fieldName: string) => {
                    this.payment[fieldName] = payment[fieldName];
                });
                this.working = false;
            }, (payment: any) => {
                this.working = false;
            });
    }

}

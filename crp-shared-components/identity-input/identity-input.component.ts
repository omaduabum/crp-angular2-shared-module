import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, forwardRef } from '@angular/core';
import { FormBuilder, FormGroup, NG_VALUE_ACCESSOR, Validators, ControlValueAccessor, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';

import {CRPService} from "../../crp-shared-services/crp.service";
import {CompanyTypeService} from "../../crp-shared-services/company-type.service";
import {CompanyClassificationService} from "../../crp-shared-services/company-classification.service";
import {NameReservationService} from "../../crp-shared-services/name-reservation.service";

@Component({
  selector: 'app-identity-input',
  templateUrl: './identity-input.component.html',
  styleUrls: ['./identity-input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => IdentityInputComponent),
      multi: true
    }
  ]
})
export class IdentityInputComponent implements OnInit, ControlValueAccessor {

  initialized: boolean;
  form: FormGroup;

  identityTypes: any[];
  states: any[];

  constructor(
    private fb: FormBuilder,
    private crpService: CRPService) {
    this.form = this.fb.group({
      identityType: ['', Validators.required],
      identityNumber: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-zA-Z0-9]+[-/a-zA-Z0-9]*$/),
        Validators.maxLength(255)
      ])],
      identityState: ['']
    });
  }

  ngOnInit() {
    Observable.merge(
      this.crpService.fetchStates()
        .map((data: any[]) => ['states', data]),
      this.crpService.fetchIdentityTypes()
        .map((data: any[]) => ['identityTypes', data])
    )
      .reduce((out: any, data: any[]): any => {
        out[data[0]] = data[1];
        return out;
      }, {})
      .subscribe((data: any) => {

        this.states = data['states'];
        this.identityTypes = data['identityTypes'];
        this.initialized = true;
      });
  }

  isInvalid(field: string): boolean {
    const control: AbstractControl = this.form.controls[field];
    return control.touched && control.invalid;
  } writeValue(obj: any): void {
    this.form.patchValue(obj || {});
  }

  registerOnChange(fn: any): void {
    this.form.valueChanges.subscribe((data: any) => {
      this.handleChange(fn);
    });
    this.handleChange(fn);
  }

  private handleChange(fn: any) {
    if (this.form.invalid) {
      fn(null);
    } else {
      const value = JSON.parse(JSON.stringify(this.form.value));
      value.identityType = value.identityType.name;
      fn(value);
    }
  }

  registerOnTouched(fn: any): void { }

  setDisabledState?(isDisabled: boolean): void { }

  equals(a: any, b: any): boolean {
    if (!a || !b || !a.name || !b.name) {
      return false;
    }
    return a.name.toLowerCase() === b.name.toLowerCase();
  }

}

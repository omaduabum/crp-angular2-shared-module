import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'affiliateName'
})
export class AffiliateNamePipe implements PipeTransform {

  transform(affiliate: any, args?: any): any {
    if (!affiliate) {
      return '';
    }
    if (affiliate.isCorporate) {
      return affiliate.corporationName;
    }
    return `${affiliate.surname} ${affiliate.firstname} ${affiliate.otherName || ''}`;
  }

}

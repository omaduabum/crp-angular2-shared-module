import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'companyAddress'
})
export class CompanyAddressPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let address: string = value.address;
    if (value.state) {
      address += ` (${value.city}, ${value.state})`;
    }
    return address;
  }

}

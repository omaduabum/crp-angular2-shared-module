import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { environment } from 'app/../environments/environment';

@Component({
  selector: 'app-invoice-payment',
  templateUrl: './invoice-payment.component.html',
  styleUrls: ['./invoice-payment.component.css']
})
export class InvoicePaymentComponent implements OnInit {

  @Output() paymentStarted = new EventEmitter();

  @Input() invoice: any;
  paymentSummary: any;
  paymentItems: any[];

  constructor() { }

  ngOnInit() {
    this.paymentSummary = this.invoice.payment;
    this.paymentItems = this.invoice.details;
  }

  crpUrl(pathSegments?: string[]): string {
    if (!pathSegments) {
      return environment.crpBaseUrl;
    }
    return [environment.crpBaseUrl === '/' ? '' : environment.crpBaseUrl].concat(pathSegments).join('/');
  }

  handlePaymentStart() {
    this.paymentStarted.emit();
  }

}

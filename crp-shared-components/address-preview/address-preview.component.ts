import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-address-preview',
  templateUrl: './address-preview.component.html',
  styleUrls: ['./address-preview.component.css']
})
export class AddressPreviewComponent implements OnInit {

  @Input() address: any;

  constructor() { }

  ngOnInit() {
  }

  hasAddress() {
    return !!this.address;
  }

  getProperty(name: string) {
    return this.hasAddress() ? this.address[name] : 'N/A';
  }

  hasProperty(name: string) {
    return this.hasAddress() && this.address[name];
  }

  getState() {
    return this.hasAddress() && this.address.state ? this.address.state.name : 'N/A';
  }

  getCountry() {
    return this.hasAddress() && this.address.country ? this.address.country.name : 'N/A';
  }

}
